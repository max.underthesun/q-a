module.exports = {
  // extends: ["eslint-config-airbnb-base", "plugin:prettier/recommended"],
  extends: ["eslint-config-airbnb-base"],

  env: {
    browser: true
  },

  parser: "babel-eslint",

  settings: {
    "import/resolver": {
      webpack: {
        config: {
          resolve: {
            modules: ["frontend", "node_modules"]
          }
        }
      }
    }
  },

  rules: {
    "no-plusplus": [2, { allowForLoopAfterthoughts: true }],
    "no-underscore-dangle": 'off'
  },

  overrides: [
    {
      'files': ['questions.js', 'listeners.js'],
      'rules': {
        'no-var': 'off',
        'prefer-template': 'off',
        'prefer-destructuring': 'off',
        'object-shorthand': 'off',
        'wrap-iife': ['error', 'inside'],
        'func-names': 'off',
        'prefer-arrow-callback': 'off'
      }
    }
  ]
};


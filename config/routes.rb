Rails.application.routes.draw do
  devise_for :admins, path: 'admins', skip: [:registrations]
  devise_for :users, path: 'users'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'home#home'

  scope :admin do
    devise_scope :admin do
      authenticated :admin do
        root 'admin/home#home', as: :admin_root
      end

      unauthenticated :admin do
        root 'devise/sessions#new', as: :unauthenticated_admin_root
      end
    end
  end

  namespace :admin do
    resources :questions, only: %i[index show edit update destroy]
  end

  resources :questions do
    resources :answers, shallow: true
  end
end

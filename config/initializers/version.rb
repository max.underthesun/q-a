# frozen_string_literal: true

module QA
  class Application < Rails::Application
    VERSION = '1.0.0'
  end
end

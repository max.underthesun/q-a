class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@frozen-thicket-19075.herokuapp.com'
  layout 'mailer'
end

# frozen_string_literal: true

class DeviseMailer < Devise::Mailer
  default from: 'no-reply@frozen-thicket-19075.herokuapp.com'
end

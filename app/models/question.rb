# frozen_string_literal: true

class Question < ApplicationRecord
  belongs_to :user
  has_many :answers, dependent: :restrict_with_error

  validates :title, presence: true
end

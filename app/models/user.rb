# frozen_string_literal: true

class User < ApplicationRecord
  has_many :questions, dependent: :restrict_with_error
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :confirmable,
         :validatable
end

# frozen_string_literal: true

module BootstrapFlashHelper
  ALERT_TYPES = %i[success info warning danger].freeze unless const_defined?(:ALERT_TYPES)

  def bootstrap_flash(options = {})
    flash_messages = []
    flash.each do |type, message|
      # Skip empty messages, e.g. for devise messages set to nothing in a locale file.
      next if message.blank?

      type = define_type(type)
      next unless ALERT_TYPES.include?(type)

      tag_options = tag_options(type, options).merge(options)
      flash_messages += flash_messages(message, tag_options)
    end

    flash_messages.join("\n").html_safe
  end

  private

  def flash_messages(message, tag_options)
    flash_messages = []

    Array(message).each do |msg|
      text = content_tag(:div, close_button + msg, tag_options)
      flash_messages << text if msg
    end

    flash_messages
  end

  def define_type(type)
    type = type.to_sym
    return :success if type == :notice
    return :danger  if type == :alert
    return :danger  if type == :error

    type
  end

  def tag_options(type, options)
    tag_class = options.extract!(:class)[:class]

    {
      class: "alert alert-#{type} #{tag_class}",
      role: "alert"
    }
  end

  def close_button
    button_options = {
      type: "button",
      class: "close",
      data: { dismiss: "alert" },
      aria: { label: "Close"}
    }

    content_tag(:button, button_options) do
      content_tag(:span, class: 'mg-l-10', aria: { hidden: true }) do
        raw('&times;')
      end
    end
  end
end

# frozen_string_literal: true

class AnswerPolicy < ApplicationPolicy
  def new?
    create?
  end

  def create?
    user&.persisted?
  end
end

# frozen_string_literal: true

class QuestionPolicy < ApplicationPolicy
  alias question record

  def new?
    create?
  end

  def create?
    user&.persisted?
  end

  def edit?
    update?
  end

  def update?
    user == question.user || admin
  end

  def destroy?
    user == question.user || admin&.super?
  end
end

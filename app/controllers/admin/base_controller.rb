# frozen_string_literal: true

class Admin::BaseController < ApplicationController
  before_action :authenticate_admin!

  private

  def base_layout
    'admin'
  end
end

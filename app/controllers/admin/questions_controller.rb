# frozen_string_literal: true

class Admin::QuestionsController < Admin::BaseController
  before_action :find_question, only: %i[show edit update destroy]
  skip_after_action :verify_authorized, only: %i[index show]

  def index
    @questions = Question.all
  end

  def show; end

  def edit
    authorize @question
  end

  def update
    authorize @question
    respond_to do |format|
      if @question.update(question_params)
        flash[:notice] = t('.success')
        format.html { redirect_to [:admin, @question] }
      else
        format.html { render :edit }
      end
      format.js { render 'admin/questions/update' }
    end
  end

  def destroy
    authorize @question
    @question.destroy
    flash[:notice] = t('.success')
    respond_to do |format|
      format.html do
        redirect_to admin_questions_path
      end

      format.js { render 'admin/questions/destroy' }
    end
  end

  private

  def find_question
    @question = Question.find(params[:id])
  end

  def question_params
    params.require(:question).permit(:title, :body)
  end
end

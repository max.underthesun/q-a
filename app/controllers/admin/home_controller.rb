# frozen_string_literal: true

class Admin::HomeController < Admin::BaseController
  skip_after_action :verify_authorized

  def home; end
end

# frozen_string_literal: true

class QuestionsController < BaseController
  skip_before_action :authenticate_user!, only: %i[index show]
  skip_after_action :verify_authorized, only: %i[index show]

  before_action :find_question, only: %i[show edit update destroy]

  def index
    @questions = Question.all
  end

  def show
    @answer = Answer.new
  end

  def new
    @question = Question.new
    authorize @question
  end

  def create
    @question = current_user.questions.new(question_params)
    authorize @question
    if @question.save
      flash[:notice] = t('.success')
      redirect_to @question
    else
      render :new
    end
  end

  def edit
    authorize @question
  end

  def update
    authorize @question
    respond_to do |format|
      if @question.update(question_params)
        flash[:notice] = t('.success')
        format.html { redirect_to @question }
      else
        format.html { render :edit }
      end
      format.js
    end
  end

  def destroy
    authorize @question
    @question.destroy
    flash[:notice] = t('.success')
    respond_to do |format|
      format.html do
        redirect_to questions_path
      end

      format.js
    end
  end

  private

  def question_params
    params.require(:question).permit(:title, :body)
  end

  def find_question
    @question = Question.find(params[:id])
  end
end

# frozen_string_literal: true

class ApplicationController < ActionController::Base
  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  layout :choose_layout

  after_action :verify_authorized, unless: :devise_controller?

  private

  def choose_layout
    return 'devise' if devise_controller?

    base_layout
  end

  def pundit_user
    current_user || current_admin
  end

  def user_not_authorized(exception)
    policy_name = exception.policy.class.to_s.underscore

    flash[:error] = t("#{policy_name}.#{exception.query}", scope: "pundit", default: :default)
    redirect_to(request.referrer || root_path)
  end

  def base_layout
    'application'
  end
end

# frozen_string_literal: true

class HomeController < BaseController
  skip_before_action :authenticate_user!
  skip_after_action :verify_authorized

  def home; end
end

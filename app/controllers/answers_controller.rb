# frozen_string_literal: true

class AnswersController < ApplicationController
  skip_after_action :verify_authorized, only: %i[index]
  before_action :find_question, only: %i[index create]

  # for the case of page refresh after unsuccessful form submit
  def index
    redirect_to @question
  end

  def create
    @answer = @question.answers.new(answer_params)
    authorize @answer
    @answer.user = current_user
    if @answer.save
      flash[:notice] = t('.success')
      redirect_to @question
    else
      render 'questions/show'
    end
  end

  private

  def answer_params
    params.require(:answer).permit(:body)
  end

  def find_question
    @question = Question.find(params[:question_id])
  end
end

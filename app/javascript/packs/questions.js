// trying to keep vanila js style here

var _questions = (function () {
  function _closeForm(questionContainer, form, questionTitle) {
    var errorsContainer = document.querySelector('.errors');
    if (errorsContainer) {
      errorsContainer.innerHTML = '';
    }
    questionContainer.removeChild(form);
    questionContainer.insertBefore(questionTitle, questionContainer.firstChild);
  }

  function _openForm(event) {
    var link = event.target.closest('a.edit-question-link');
    var id = link.dataset.id;
    var form = document.querySelector('.inline-form.form-template').cloneNode(true);
    var path = form.dataset.path;
    var questionContainer = document.querySelector('#question-' + id + '-container');
    var questionTitle = questionContainer.querySelector('.question-title');

    event.preventDefault();
    form.setAttribute('action', path + id);
    form.querySelector('button.close-form').addEventListener('click', function (e) {
      e.preventDefault();
      _closeForm(questionContainer, form, questionTitle);
    });
    form.querySelector('#question_title').value = questionTitle.textContent;
    form.classList.remove('form-template');
    questionContainer.removeChild(questionTitle);
    questionContainer.insertBefore(form, questionContainer.firstChild);
  }

  function edit() {
    var editLinks = document.querySelectorAll('.edit-question-link');
    var i;
    for (i = 0; i < editLinks.length; ++i) {
      editLinks[i].addEventListener('click', _openForm);
    }
  }

  return {
    edit: edit,
  };
})();

module.exports = _questions;

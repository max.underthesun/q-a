**!!! This project and this file is under regular update !!!**

# [q&a]

Simple question and answer application. Developed for demonstration purposes.
Main goal of this application is to show a range of technologies and solutions I am using in my work.
You can check the list of features implemented below. I've also added a list of features planned.

The application itself is <a href="https://frozen-thicket-19075.herokuapp.com" target="_blank">hosted on Heroku</a>. Fill free to check it.
It is possible to register as a user with you email. To access admins part use `admin@admin.admin` as login and `12345678` for the pass.

Here check <a href="https://gitlab.com/max.underthesun/q-a/-/boards"  target="_blank">GitLab Issue Boards</a> for this project

### Contents
- [Technology stack](#technology-stack)
- [Features implemented](#features-implemented)
- [Features planned](#features-planned)
- [My environment and habits](#my-environment-and-habits)

## Technology stack
- Ruby 2.6.5
- Ruby On Rails 6.0.1
- Node JS 10.16.3
- Yarn 1.19.0
- PostgreSQL 10.10
- Gems:
  - <a href="https://github.com/plataformatec/devise"  target="_blank">devise</a> | Authentication
  - <a href="https://github.com/varvet/pundit"  target="_blank">pundit</a> | Authorization
  - <a href="https://github.com/slim-template/slim-rails"  target="_blank">slim-rails</a> and ERB | HTML templating
  - <a href="https://github.com/ged/ruby-pg" target="_blank">pg</a> | Ruby interface for PostgreSQL RDBMS
  - rspec-rails, factory_bot_rails, capybara, shoulda-matchers | Testing
- HTML-CSS-JS theme <a href="https://themeforest.net/item/bracket-responsive-bootstrap-3-admin-template/6894362" target="_blank">bracket</a>

## Features implemented
- two Devise models (User & Admin) with separate layouts (admin routes namespaced)
- using AJAX (through `erb.js` views rails internal mechanics) for updating question title and deleting question on questions#index page
- add `gitlab-ci.yml` configuration file to the project to exploit GitLab Continuous Integration
- using make and overmind to start project processes

## Features planned
- use AJAX for adding, editing and deleting answers on the question page (through HTTP requests from client side via JS)
- set file upload with ActiveStorage and AWS
- add ReactJS front-end for users part of the application with front-back communication through Rest API
- search through all the questions, answers and comments
- use jQuery Data Tables for users and admins index pages (sorting, filtering, searching)
- use ActionCable for updating questions, answers and comments indices "on the fly"

## My environment and habits
- Ubuntu 18.04 LTS | OS
- Neovim, Atom | editors
- Git | distributed version control system
- <a href="https://github.com/rubocop-hq/rubocop" target="_blank">Rubocop</a> | Ruby static code analyzer and code formatter
- <a href="https://nvie.com/posts/a-successful-git-branching-model" target="_blank">I like this git flow</a> | A successful Git branching model
- <a href="https://github.com/DarthSim/overmind" target="_blank">Overmind</a> | process manager for Procfile-based applications

# frozen_string_literal: true

require 'system/integration_helper'
require 'system/shared/questions/all_questions_titles'

RSpec.describe 'admin can see questions index', type: :system do
  let!(:admin) { create(:admin) }
  let!(:questions) { create_list(:question, 3) }

  context 'when logged' do
    before do
      sign_in admin
    end

    include_examples 'see questions titles'
  end
end

# frozen_string_literal: true

require 'system/integration_helper'
require 'system/shared/questions/question_title_and_body.rb'

RSpec.describe 'admin can see question show', type: :system do
  let!(:question) { create(:question) }

  context 'when logged' do
    let!(:admin) { create(:admin) }

    before { sign_in admin }

    include_examples 'able to see question title and body'
  end
end

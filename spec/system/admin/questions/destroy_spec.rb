# frozen_string_literal: true

require 'system/integration_helper'

RSpec.shared_examples 'do not have delete question links' do
  scenario 'do not see "delete question" link' do
    expect(page).to_not have_css("a[data-method='delete'][href='#{admin_question_path(question)}']")
  end
end

RSpec.shared_examples 'question destroyed' do
  scenario 'see question destroyed message' do
    expect(page).to have_content(t('questions.destroy.success'))
  end

  scenario 'do not see question title on question index page' do
    expect(current_path).to eq admin_questions_path
    expect(page).to_not have_content(question.title)
  end
end

RSpec.describe 'superadmin can destroy any question', type: :system do
  let!(:questions) { create_list(:question, 3) }
  let!(:question) { questions.sample }
  let(:superadmin) { create(:admin, role: :super) }
  let!(:admin) { create(:admin, :not_super) }

  context 'when super' do
    before do
      sign_in superadmin
    end

    context 'when on question page' do
      before do
        visit admin_question_path(question)
        find("a[data-method='delete'][href='#{admin_question_path(question)}']").click
      end

      include_examples 'question destroyed'
    end

    context 'when on questions index page', js: true do
      before do
        visit admin_questions_path
        accept_confirm do
          find("a[data-method='delete'][href='#{admin_question_path(question)}']").click
        end
      end

      include_examples 'question destroyed'
    end
  end

  context 'when not super' do
    before do
      sign_in admin
    end

    context 'when on question page' do
      before do
        visit admin_question_path(question)
      end
      include_examples 'do not have delete question links'
    end

    context 'when on questions index page' do
      before do
        visit admin_questions_path
      end
      include_examples 'do not have delete question links'
    end
  end
end

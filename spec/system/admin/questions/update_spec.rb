# frozen_string_literal: true

require 'system/integration_helper'

RSpec.shared_examples 'question edited' do
  scenario 'should see "question edited" message' do
    expect(page).to have_content(t('admin.questions.update.success'))
  end

  scenario 'should see edited question title' do
    expect(page).to have_content(edited_question.title)
  end
end

RSpec.shared_examples 'question not edited' do
  scenario 'should not see "question edited" message' do
    expect(page).to_not have_content(t('admin.questions.update.success'))
  end

  scenario 'should see error message' do
    message = question.errors.full_message(:title, t('errors.messages.blank'))
    expect(page).to have_content message
  end
end

RSpec.describe 'admin can edit any question', type: :system do
  let(:author) { create(:user, :confirmed) }
  let(:admin) { create(:admin) }
  let!(:questions) { create_list(:question, 3, user: author) }
  let!(:question) { questions.sample }

  context 'when logged' do
    let(:edited_question) { build(:question) }
    let(:invalid_question) { build(:question, :invalid) }

    context 'on question edit page' do
      before do
        sign_in admin
        visit admin_question_path(question)
        find("a[href='#{edit_admin_question_path(question)}']", text: t('shared.edit')).click
      end

      context 'with valid attributes should be able to edit question' do
        before do
          find('input#question_title').set(edited_question.title)
          find('textarea#question_body').set(edited_question.body)
          find('input[type="submit"]').click
        end

        include_examples 'question edited'

        scenario 'should see edited question body' do
          expect(page).to have_content(edited_question.body)
        end
      end

      context 'with invalid attributes should not be able to edit question' do
        before do
          find('input#question_title').set(invalid_question.title)
          find('textarea#question_body').set(invalid_question.body)
          find('input[type="submit"]').click
        end

        include_examples 'question not edited'

        scenario 'should see unedited question title on question page' do
          visit admin_question_path(question)
          expect(page).to have_content(question.title)
        end

        scenario 'should see unedited question body on question page' do
          visit admin_question_path(question)
          expect(page).to_not have_content(edited_question.body)
          expect(page).to have_content(question.body)
        end
      end
    end

    context 'on questions index page', js: true do
      before do
        sign_in admin
        visit admin_questions_path
        find("a.edit-question-link[href='#{edit_admin_question_path(question)}']").click
      end

      context 'with valid attributes should be able to edit question' do
        before do
          find('input#question_title').set(edited_question.title)
          find('input[type="submit"]').click
        end

        include_examples 'question edited'
      end

      context 'with invalid attributes should not be able to edit question' do
        before do
          find('input#question_title').set(invalid_question.title)
          find('input[type="submit"]').click
        end

        include_examples 'question not edited'

        scenario 'should see unedited question title on questions index page' do
          visit admin_questions_path
          expect(page).to have_content(question.title)
        end
      end
    end
  end
end

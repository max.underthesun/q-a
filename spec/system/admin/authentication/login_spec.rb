# frozen_string_literal: true

require 'system/integration_helper'

RSpec.describe 'admin login', type: :system do
  context 'when registered admin able to sign_in' do
    let!(:admin) { create(:admin) }

    scenario 'redirected to root_path' do
      sign_in admin
      expect(current_path).to eq admin_root_path
    end

    scenario 'see "signed_in" message' do
      sign_in admin
      expect(page).to have_content t('devise.sessions.signed_in')
    end
  end

  context 'when unregistered admin not able to sign_in' do
    let(:admin) { build(:admin) }

    scenario 'not redirected to root_path' do
      sign_in admin
      expect(current_path).to_not eq root_path
      expect(current_path).to eq new_admin_session_path
    end

    scenario 'see "invalid" message' do
      sign_in admin
      expect(page).to_not have_content t('devise.sessions.signed_in')
      expect(page).to have_content t('devise.failure.invalid', authentication_keys: 'Email')
    end
  end
end

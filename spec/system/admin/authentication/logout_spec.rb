# frozen_string_literal: true

require 'system/integration_helper'

RSpec.describe 'admin can logout', type: :system do
  context 'when logged' do
    let!(:admin) { create(:admin) }

    before do
      sign_in admin
    end

    scenario 'see "logout" link' do
      expect(page).to have_link(t('devise.shared.logout'), href: destroy_admin_session_path)
    end

    scenario 'do not see "login" link' do
      expect(page).to_not have_link(t('devise.shared.login'), href: new_admin_session_path)
    end

    scenario 'able to logout' do
      find("a[href='#{destroy_admin_session_path}']", text: t('devise.shared.logout')).click
      expect(current_path).to eq root_path
      expect(page).to have_content(t('devise.sessions.signed_out'))
    end
  end

  context 'when unlogged' do
    scenario 'do not see "logout" link' do
      expect(page).to_not have_link(t('devise.shared.logout'), href: destroy_admin_session_path)
    end
  end
end

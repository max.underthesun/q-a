# frozen_string_literal: true

require 'rails_helper'
require 'support/integration_methods'

RSpec.configure do |config|
  config.include IntegrationMethods, type: :system
end

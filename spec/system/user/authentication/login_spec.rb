# frozen_string_literal: true

require 'system/integration_helper'

RSpec.describe 'user login', type: :system do
  scenario 'when unlogged user able to see "login" link' do
    visit root_path
    expect(page).to have_link(t('devise.shared.login'), href: new_user_session_path)
  end

  context 'when registered and confirmed user able to sign_in' do
    let(:user) { create(:user, :confirmed) }

    scenario 'redirected to root_path' do
      sign_in user
      expect(current_path).to eq root_path
    end

    scenario 'see "signed_in" message' do
      sign_in user
      expect(page).to have_content t('devise.sessions.signed_in')
    end

    scenario 'not able to see "login" link' do
      expect(page).to_not have_link(t('devise.shared.login'), href: new_user_session_path)
    end
  end

  context 'when registered and unconfirmed user not able to sign_in' do
    let(:user) { create(:user) }

    scenario 'not redirected to root_path' do
      sign_in user
      expect(current_path).to_not eq root_path
      expect(current_path).to eq new_user_session_path
    end

    scenario 'see "unconfirmed" message' do
      sign_in user
      expect(page).to_not have_content t('devise.sessions.signed_in')
      expect(page).to have_content t('devise.failure.unconfirmed')
    end
  end

  context 'when unregistered user not able to sign_in' do
    let(:user) { build(:user) }

    scenario 'not redirected to root_path' do
      sign_in user
      expect(current_path).to_not eq root_path
      expect(current_path).to eq new_user_session_path
    end

    scenario 'see "invalid" message' do
      sign_in user
      expect(page).to_not have_content t('devise.sessions.signed_in')
      expect(page).to have_content t('devise.failure.invalid', authentication_keys: 'Email')
    end
  end
end

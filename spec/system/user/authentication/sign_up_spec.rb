# frozen_string_literal: true

require 'system/integration_helper'

RSpec.describe 'user sign up', type: :system do
  context 'when unlogged user able to sign up' do
    scenario 'see "sign up" link' do
      visit root_path
      within('#header') do
        expect(page).to have_link(t('devise.shared.signup'), href: new_user_registration_path)
      end
    end

    scenario 'can visit new_user_registration page' do
      visit root_path
      find("a[href='#{new_user_registration_path}']", text: t('devise.shared.signup')).click
      expect(current_path).to eq new_user_registration_path
    end

    scenario 'after registering see "signed_up_but_unconfirmed" message, confirmed can sign in' do
      user = build(:user)
      visit new_user_registration_path
      find("input#user_email").set(user.email)
      find("input#user_password").set(user.password)
      find("input#user_password_confirmation").set(user.password_confirmation)
      find("input[type='submit'][value='#{t('devise.shared.signup')}']").click

      expect(page).to have_content(t('devise.registrations.signed_up_but_unconfirmed'))

      User.find_by(email: user.email).confirm
      sign_in user
      expect(page).to have_content t('devise.sessions.signed_in')
    end
  end

  context 'when logged user unable to sign up' do
    scenario 'can not see "sign up" link' do
      user = create(:user, :confirmed)
      sign_in user
      expect(current_path).to eq root_path
      expect(page).to_not have_link(t('devise.shared.signup'), href: new_user_registration_path)
    end
  end
end

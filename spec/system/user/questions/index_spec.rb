# frozen_string_literal: true

require 'system/integration_helper'
require 'system/shared/questions/all_questions_titles'

RSpec.describe 'user can see question index', type: :system do
  let!(:user) { create(:user) }
  let!(:admin) { create(:admin) }
  let!(:questions) { create_list(:question, 3) }

  context 'when unlogged' do
    include_examples 'see questions titles'
  end

  context 'when logged' do
    before do
      sign_in user
    end

    include_examples 'see questions titles'
  end
end

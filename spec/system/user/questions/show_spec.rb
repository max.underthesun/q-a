# frozen_string_literal: true

require 'system/integration_helper'
require 'system/shared/questions/question_title_and_body.rb'

RSpec.describe 'user can see question show', type: :system do
  let!(:question) { create(:question) }

  context 'when unlogged' do
    include_examples 'able to see question title and body'
  end

  context 'when logged' do
    let!(:user) { create(:user, :confirmed) }

    before { sign_in user }

    include_examples 'able to see question title and body'
  end
end

# frozen_string_literal: true

require 'system/integration_helper'

RSpec.describe 'question create', type: :system do
  context 'logged user' do
    let!(:user) { create(:user, :confirmed) }

    before do
      sign_in user
      visit new_question_path
    end

    it 'able to visit questions new page' do
      expect(current_path).to eq new_question_path
    end

    context 'when valid attributes' do
      let(:question) { build(:question) }

      before do
        find('input#question_title').set(question.title)
        find('input[type="submit"]').click
      end

      it 'see "question created" message on form submit' do
        expect(page).to have_content t('questions.create.success')
      end

      it 'see question title on questions index page' do
        expect(page).to have_content question.title
      end
    end

    context 'when invalid attributes' do
      let(:question) { build(:question, :invalid) }

      before do
        find('input#question_title').set(question.title)
        find('input[type="submit"]').click
      end

      it 'do not see "question created" message on form submit' do
        expect(page).to_not have_content t('questions.create.success')
      end

      it 'see error message' do
        message = question.errors.full_message(:title, t('errors.messages.blank'))
        expect(page).to have_content message
      end
    end
  end

  context 'unlogged user' do
    it 'unable visit new question page' do
      visit new_question_path
      expect(current_path).to eq new_user_session_path
      expect(page).to have_content(t('devise.failure.unauthenticated'))
    end
  end
end

# frozen_string_literal: true

require 'system/integration_helper'

RSpec.shared_examples 'do not have delete question links' do
  context 'when on question page' do
    before do
      visit question_path(question)
    end

    scenario 'do not see "delete question" link' do
      expect(page).to_not have_css("a[data-method='delete'][href='#{question_path(question)}']")
    end
  end

  context 'when on questions index page' do
    before do
      visit questions_path
    end

    scenario 'do not see "delete question" link' do
      expect(page).to_not have_css("a[data-method='delete'][href='#{question_path(question)}']")
    end
  end
end

RSpec.shared_examples 'question destroyed' do
  scenario 'see question destroyed message' do
    expect(page).to have_content(t('questions.destroy.success'))
  end

  scenario 'do not see question title on question index page' do
    expect(current_path).to eq questions_path
    expect(page).to_not have_content(question.title)
  end
end

RSpec.describe 'user can destroy his question', type: :system do
  let(:author) { create(:user, :confirmed) }
  let(:user) { create(:user, :confirmed) }
  let!(:questions) { create_list(:question, 3, user: author) }
  let!(:question) { questions.sample }

  context 'when unlogged' do
    include_examples 'do not have delete question links'
  end

  context 'when logged and author' do
    before do
      sign_in author
    end

    context 'when on question page' do
      before do
        visit question_path(question)
        find("a[data-method='delete'][href='#{question_path(question)}']").click
      end

      include_examples 'question destroyed'
    end

    context 'when on questions index page', js: true do
      before do
        visit questions_path
        accept_confirm do
          find("a[data-method='delete'][href='#{question_path(question)}']").click
        end
      end

      include_examples 'question destroyed'
    end
  end

  context 'when logged and not author' do
    before do
      sign_in user
    end

    include_examples 'do not have delete question links'
  end
end

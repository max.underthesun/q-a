# frozen_string_literal: true

require 'system/integration_helper'

RSpec.describe 'logged user can answer any question' do
  let!(:question) { create(:question) }
  let!(:answers) { create_list(:answer, 3, question: question) }
  let(:path) { question_answers_path(question) }

  context 'unlogged user' do
    before do
      visit question_path(question)
    end

    scenario 'should not see new_answer form' do
      expect(page).to_not have_css("form[action='#{path}'][method='post']")
    end
  end

  context 'logged user' do
    let(:user) { create(:user, :confirmed) }

    before do
      sign_in user
      visit question_path(question)
    end

    scenario 'should see new_answer form' do
      expect(page).to have_css("form[action='#{path}'][method='post']")
    end

    context 'with valid attributes' do
      let(:answer) { build(:answer) }

      before do
        within("form[action='#{path}']") do
          find('textarea#answer_body').set(answer.body)
          find('input[type="submit"]').click
        end
      end

      scenario 'should see "anwer created" message' do
        expect(page).to have_content(t('answers.create.success'))
      end

      scenario 'should see answer body in the list of question answers' do
        within(".question-#{question.id}-answers") do
          expect(page).to have_content(answer.body)
        end
      end
    end

    context 'with invalid attributes' do
      let(:invalid_answer) { build(:answer, :short_body) }

      before do
        within("form[action='#{path}']") do
          find('textarea#answer_body').set(invalid_answer.body)
          find('input[type="submit"]').click
        end
      end

      scenario 'should not see "answer created" message' do
        expect(page).to_not have_content(t('answers.create.success'))
      end

      scenario 'should not see answer body in the list of question answers' do
        within(".question-#{question.id}-answers") do
          expect(page).to_not have_content(invalid_answer.body)
        end
      end

      scenario 'see error message' do
        message = invalid_answer.errors.full_message(:body, t('errors.messages.too_short', count: 20))
        expect(page).to have_content message
      end
    end
  end
end

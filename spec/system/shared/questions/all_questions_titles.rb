# frozen_string_literal: true

RSpec.shared_examples 'see questions titles' do
  it 'see all questions titles on questions index page' do
    visit questions_path
    questions.each do |question|
      expect(page).to have_content(question.title)
    end
  end
end

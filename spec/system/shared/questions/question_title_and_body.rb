# frozen_string_literal: true

RSpec.shared_examples 'able to see question title and body' do
  before { visit question_path(question) }

  it 'able to visit question page and see question title' do
    expect(page).to have_content(question.title)
  end

  it 'able to visit question page and see question body' do
    expect(page).to have_content(question.body)
  end
end



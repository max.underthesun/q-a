# frozen_string_literal: true

FactoryBot.define do
  factory :admin do
    sequence(:email) { |n| "user-#{n}@example.com" }
    password { '0123456789' }
    password_confirmation { '0123456789' }
    sequence(:role) { Admin.roles.keys.sample }

    trait :not_super do
      role { Admin.roles.keys.grep(/^(?!super)/).sample }
    end
  end
end

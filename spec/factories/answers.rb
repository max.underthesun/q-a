FactoryBot.define do
  factory :answer do
    sequence(:body) { |n| "body for the answer-#{n}" }
    question
    user

    trait :short_body do
      body { 'short body' }
    end
  end
end

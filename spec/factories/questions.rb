# frozen_string_literal: true

FactoryBot.define do
  factory :question do
    sequence(:title) { |n| "Question-#{n} Title" }
    user
    sequence(:body) { |n| "question #{n} body" }

    trait :invalid do
      title { nil }
    end
  end
end

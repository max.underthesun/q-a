# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "user-#{n}@example.com" }

    password { '0123456789' }
    password_confirmation { '0123456789' }

    trait :confirmed do
      confirmed_at { Time.current }
      confirmation_token { rand(100_000).to_s }
    end
  end
end

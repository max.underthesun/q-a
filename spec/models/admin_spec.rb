# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Admin, type: :model do
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password) }
  it { should validate_length_of(:password).is_at_least(8) }
  it { should validate_length_of(:password).is_at_most(128) }

  it { should define_enum_for(:role).with_values(%i[super manager editor]) }
  it { should validate_presence_of(:role) }
end

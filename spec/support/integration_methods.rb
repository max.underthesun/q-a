# frozen_string_literal: true

module IntegrationMethods
  def sign_in(resource, email: resource.email, password: resource.password)
    resource_name = resource.class.to_s.underscore
    visit send("new_#{resource_name}_session_path")
    fill_in t("activerecord.attributes.#{resource_name}.email"), with: email
    fill_in t("activerecord.attributes.#{resource_name}.password"), with: password
    click_on t('devise.sessions.new.submit')
  end
end

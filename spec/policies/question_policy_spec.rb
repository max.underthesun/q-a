# frozen_string_literal: true

require 'rails_helper'

RSpec.describe QuestionPolicy do
  let(:author) { create(:user, :confirmed) }
  let(:question) { build(:question, user: author) }

  subject { described_class }

  context 'when admin' do
    let(:superadmin) { create(:admin, role: :super) }
    let(:admin) { create(:admin, :not_super) }

    permissions :destroy? do
      it 'grants access to superadmin' do
        expect(subject).to permit(superadmin, question)
      end
    end

    permissions :destroy? do
      it 'denies access to any admin but super' do
        expect(subject).not_to permit(admin, question)
      end
    end

    permissions :edit?, :update? do
      it 'grants access to any admin' do
        expect(subject).to permit(superadmin, question)
        expect(subject).to permit(admin, question)
      end
    end
  end

  context 'when user' do
    let(:user) { create(:user, :confirmed) }

    permissions :new?, :create? do
      it 'grants access to any user' do
        expect(subject).to permit(user, question)
      end
    end

    permissions :new?, :create? do
      it 'denies access to unlogged user' do
        expect(subject).not_to permit(nil, question)
      end
    end

    permissions :destroy? do
      it 'grants access to author' do
        expect(subject).to permit(author, question)
      end
    end

    permissions :destroy? do
      it 'denies access to not author' do
        expect(subject).not_to permit(user, question)
      end
    end

    permissions :edit?, :update? do
      it 'grants access to author' do
        expect(subject).to permit(author, question)
      end
    end

    permissions :edit?, :update? do
      it 'denise access to not author' do
        expect(subject).not_to permit(user, question)
      end
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AnswerPolicy do
  subject { described_class }

  context 'when user' do
    let(:user) { create(:user, :confirmed) }

    permissions :new?, :create? do
      it 'denies access to unlogged user' do
        expect(subject).not_to permit(nil, Answer)
      end
    end

    permissions :new?, :create? do
      it 'grants access to any user' do
        expect(subject).to permit(user, Answer)
      end
    end
  end
end
